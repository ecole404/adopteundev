# Submodule
- FRONT => https://gitlab.com/ecole404/adopteunfront
- ADMIN => https://gitlab.com/ecole404/adopteunadmin
- API => https://gitlab.com/ecole404/adopteuneapi
# Clone
- First clone
```console
git clone --recurse-submodules https://gitlab.com/ecole404/adopteundev.git
```
- If project already clone without `--recurse-submodules`
```console
git submodule init
git submodule update
```
# More info
https://git-scm.com/book/fr/v2/Utilitaires-Git-Sous-modules
